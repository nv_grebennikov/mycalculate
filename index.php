<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 19.08.16
 * Time: 21:38
 */

switch ($_SERVER['REQUEST_URI']) {
    case '/';
        $file_name = 'main';
        break;
    case '/mortgage';
        $file_name = 'mortgage';
        break;
    case '/consumer';
        $file_name = 'consumer';
        break;
    case '/auto';
        $file_name = 'auto';
        break;
    case '/business';
        $file_name = 'business';
        break;
    default:
        header("HTTP/1.x 404 Not Found");
        $file_name = 'error';
}

//var_dump($file_name);
//exit();

require 'const.php';
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php switch ($_SERVER['REQUEST_URI']) {
        case '/';
            echo '<title>' . META_TITLE_MAIN . '</title>',
                 '<meta name="Description" content="' . META_DESCRIPTION_MAIN . '"/>',
                 '<meta name="Keywords" content="' . META_KEYWORDS_MAIN . '"/>';
            break;
        case '/mortgage';
            echo '<title>' . META_TITLE_MORTGAGE . '</title>',
                '<meta name="Description" content="' . META_DESCRIPTION_MORTGAGE . '"/>',
                '<meta name="Keywords" content="' . META_KEYWORDS_MORTGAGE . '"/>';
            break;
        case '/consumer';
            echo '<title>' . META_TITLE_CONSUMER . '</title>',
                '<meta name="Description" content="' . META_DESCRIPTION_CONSUMER . '"/>',
                '<meta name="Keywords" content="' . META_KEYWORDS_CONSUMER . '"/>';
            break;
        case '/auto';
            echo '<title>' . META_TITLE_AUTO . '</title>',
                '<meta name="Description" content="' . META_DESCRIPTION_AUTO . '"/>',
                '<meta name="Keywords" content="' . META_KEYWORDS_AUTO . '"/>';
            break;
        case '/business';
            echo '<title>' . META_TITLE_BUSINESS . '</title>',
                '<meta name="Description" content="' . META_DESCRIPTION_BUSINESS . '"/>',
                '<meta name="Keywords" content="' . META_KEYWORDS_BUSINESS . '"/>';
            break;
        default:
            echo '<title>' . META_TITLE_ERROR . '</title>',
                '<meta name="Description" content="' . META_DESCRIPTION_ERROR . '"/>',
                '<meta name="Keywords" content="' . META_KEYWORDS_ERROR . '"/>';
    }

    ?>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/node_modules/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="/node_modules/nouislider/distribute/nouislider.min.css" rel="stylesheet">
    <link href="/css/site.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2372738827025759",
            enable_page_level_ads: true
        });
    </script>
    <meta name="yandex-verification" content="3ac0cf972240b4d0" />
</head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><?= APP_TITLE; ?></a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li <?= $file_name === 'mortgage' ? 'class="active"': null; ?>>
                            <a href="/mortgage"><?= APP_MENU_MORTGAGE; ?></a>
                        </li>
                        <li <?= $file_name === 'consumer' ? 'class="active"': null; ?>>
                            <a href="/consumer"><?= APP_MENU_CONSUMER; ?></a>
                        </li>
                        <li <?= $file_name === 'auto' ? 'class="active"': null; ?>>
                            <a href="/auto"><?= APP_MENU_AUTO; ?></a>
                        </li>
                        <li <?= $file_name === 'business' ? 'class="business"': null; ?>>
                            <a href="/business"><?= APP_MENU_BUSINESS; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <?php require 'page' . DIRECTORY_SEPARATOR . $file_name . '.php'; ?>
        </div>

        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

        <?php if ($file_name !== 'error' && $file_name !== 'main'): ?>

            <script src="node_modules/nouislider/distribute/nouislider.min.js"></script>
            <script src="node_modules/chart.js/dist/Chart.bundle.js"></script>
            <script src="js/common.js"></script>
            <script src="js/model.js"></script>
            <script src="js/controller.js"></script>

        <?php endif; ?>

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter39396265 = new Ya.Metrika({
                            id:39396265,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/39396265" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-47746323-6', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>
