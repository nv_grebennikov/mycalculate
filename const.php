<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 19.08.16
 * Time: 22:02
 */

const META_TITLE_MAIN = 'MyCalculate.ru - рассчитать кредит легко';
const META_H1_MAIN = 'Рассчитать кредит легко';
const META_DESCRIPTION_MAIN = 'MyCalculate.ru - это лёгкий и быстрый способ расчета суммы переплаты, платежа, реальной и эффективной процентной ставки по кредиту.';
const META_KEYWORDS_MAIN = 'рассчитать кредит, кредитный калькулятор, онлайн';

const META_TITLE_MORTGAGE = 'Ипотечный калькулятор - рассчитать ипотечный кредит легко.';
const META_H1_MORTGAGE = 'Ипотечный калькулятор';
const META_DESCRIPTION_MORTGAGE = 'Рассчитать ипотечный кредит, сумму переплаты, платежа, реальной и эффективной процентной ставки по кредиту легко.';
const META_KEYWORDS_MORTGAGE = 'рассчитать ипотечный кредит, ипотечный калькулятор, онлайн';

const META_TITLE_CONSUMER = 'Кредитный калькулятор - рассчитать потребительский кредит легко.';
const META_H1_CONSUMER = 'Рассчитать потребительский кредит';
const META_DESCRIPTION_CONSUMER = 'Рассчитать потребительский кредит, сумму переплаты, платежа, реальной и эффективной процентной ставки по кредиту легко.';
const META_KEYWORDS_CONSUMER = 'рассчитать потрибительский кредит, кредитный калькулятор, онлайн';

const META_TITLE_AUTO = 'Калькулятор автокредита - рассчитать кредит на авто легко.';
const META_H1_AUTO = 'Рассчитать автокредит';
const META_DESCRIPTION_AUTO = 'Рассчитать автокредит кредит, сумму переплаты, платежа, реальной и эффективной процентной ставки по кредиту легко.';
const META_KEYWORDS_AUTO = 'рассчитать автокредит, автокредит калькулятор, онлайн';

const META_TITLE_BUSINESS = 'Кредитный калькулятр - рассчитать кредит на бизнес легко.';
const META_H1_BUSINESS = 'Рассчитать кредит на бизнес';
const META_DESCRIPTION_BUSINESS = 'Рассчитать кредит на бизнес, сумму переплаты, платежа, реальной и эффективной процентной ставки по кредиту легко. ';
const META_KEYWORDS_BUSINESS = 'рассчитать кредит на бизнес, кредитный калькулятор, онлайн';

const META_TITLE_ERROR = 'Ошибка 404';
const META_H1_ERROR = '404';
const META_DESCRIPTION_ERROR = 'Ошибка 404';
const META_KEYWORDS_ERROR = 'Ошибка';

const APP_TITLE = 'Кредитный калькулятр';

const APP_MENU_CONSUMER = 'Потрибительский';
const APP_MENU_MORTGAGE = 'Ипотека';
const APP_MENU_AUTO = 'Автокредит';
const APP_MENU_CONTRIBUTION = 'Вклад';
const APP_MENU_BUSINESS = 'Бизнес';

const APP_FORM_COST = 'Стоимость покупки';
const APP_FORM_INIT_PAYMENT = 'Первоначальный взнос';
const APP_FORM_INTEREST_RATE = 'Процентная ставка';
const APP_FORM_TERM = 'Срок кредитования';
const APP_FORM_FORM_PAYMENT = 'Вид платежей';
const APP_FORM_BUTTON = 'Рассчитать';
const APP_FORM_ANNUITANT = 'Аннуитентные';
const APP_FORM_DIFFERENTIATED = 'Дифференцированные';

const APP_CREDIT_NAME_CONSUMER = 'Потрибительский кредит';
const APP_CREDIT_NAME_MORTGAGE = 'Ипотечный кредит';
const APP_CREDIT_NAME_AUTO = 'Автокредит';
const APP_CREDIT_NAME_CONTRIBUTION = 'Вклад';
const APP_CREDIT_NAME_BUSINESS = 'Кредит на бизнес';

const APP_CREDIT_LINK_NAME_CONSUMER = 'Рассчитать потрибительский кредит';
const APP_CREDIT_LINK_NAME_MORTGAGE = 'Рассчитать ипотечный кредит';
const APP_CREDIT_LINK_NAME_AUTO = 'Рассчитать автокредит';
const APP_CREDIT_LINK_NAME_CONTRIBUTION = 'Рассчитать вклад';
const APP_CREDIT_LINK_NAME_BUSINESS = 'Рассчитать кредит на бизнес';

const BUTTON_CALCULATE = 'Рассчитать';


