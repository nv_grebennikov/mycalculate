/**
 * Created by nikita on 19.08.16.
 */
'use strict';

var controller = {

    init: function () {
        console.log('controller | init');
        models.init();
    },

};

$(document).ready(function () {

    controller.init();

    $(document).on('click','#calculation',function (e) {
        e.preventDefault();
        if (models.formValidate()) {
            models.calculation();
        }
    });

    $(document).on('click','button[data-toggle="collapse"]',function (e) {
        $(this).attr('aria-expanded') === 'true' ? $(this).html('Скрыть') : $(this).html('Читать далее');
    });

    $(document).keypress(function(e) {
        models.focusNextElem(e);
    });


    $(document).on('submit',models.form,function (e) {
        e.preventDefault();
    });

    $(document).on('click',models.detailsShowSelector,function (e) {
        e.preventDefault();
        models.detailsRender();
    });

    $(document).on('click','#reload',function (e) {
        e.preventDefault();
        location.reload();
    });
});
