/**
 * Created by nikita on 20.08.16.
 */
'use strict';

var common = {

    getNoun: function(number, one, two, five) {
        number = Math.abs(number);
        number %= 100;
        if (number >= 5 && number <= 20) {
            return five;
        }
        number %= 10;
        if (number == 1) {
            return one;
        }
        if (number >= 2 && number <= 4) {
            return two;
        }
        return five;
    },

    priceEncode: function (val, thSep, dcSep) {
        val = parseFloat(val);
        val = val.toFixed(1);
        // Проверка указания разделителя разрядов
        if (!thSep) thSep = ' ';

        // Проверка указания десятичного разделителя
        if (!dcSep) dcSep = ',';

        var res = val.toString();
        var lZero = (val < 0); // Признак отрицательного числа

        // Определение длины форматируемой части
        var fLen = res.lastIndexOf('.'); // До десятичной точки
        fLen = (fLen > -1) ? fLen : res.length;

        // Выделение временного буфера
        var tmpRes = res.substring(fLen);
        var cnt = -1;
        for (var ind = fLen; ind > 0; ind--) {
            // Формируем временный буфер
            cnt++;
            if (((cnt % 3) === 0) && (ind !== fLen) && (!lZero || (ind > 1))) {
                tmpRes = thSep + tmpRes;
            }
            tmpRes = res.charAt(ind - 1) + tmpRes;
        }

        return tmpRes.replace('.', dcSep);
    },

    priceDecode: function (val) {
        return parseInt(val.replace(/\s+/g, ''));
    },

    scrollTo: function ($elem) {
        if ($elem.length > 0) {
            $('html, body').animate({
                scrollTop: $elem.offset().top
            }, 500);
        }
    }
};
