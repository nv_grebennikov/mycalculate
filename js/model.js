/**
 * Created by nikita on 19.08.16.
 */
'use strict';

var models = {

    init: function () {
        console.log('models | init');
        window.myDoughnut = new Chart(this.resultChartCtx, this.resultChartConfig);

        this.sliderInit('slider-cost','input[name="cost"]');
        this.sliderInit('slider-init-payment','input[name="init-payment"]');
        this.sliderInit('slider-interest-rate','input[name="interest-rate"]');
        this.sliderInit('slider-term','input[name="term"]');
    },

    // START FORM

    form: $('#form-credit'),

    formError: $('#form-error'),

    formValidate: function () {
        this.formError.html('');
        if (common.priceDecode(this.form.find('input[name="cost"]').val()) < this.getInitPayment()) {
            this.formError.html('<p>Первоночальный взнос не может быть больше суммы кредита.</p>')
            this.formError.removeClass('hidden');
            return false;
        }
        this.formError.addClass('hidden');
        return true;
    },

    getCost: function () {
        return common.priceDecode(this.form.find('input[name="cost"]').val())  - this.getInitPayment();
    },

    getInitPayment: function () {
        return common.priceDecode(this.form.find('input[name="init-payment"]').val());
    },

    getInterestRate: function () {
        return common.priceDecode(this.form.find('input[name="interest-rate"]').val());
    },

    getTerm: function () {
        return common.priceDecode(this.form.find('input[name="term"]').val());
    },

    getP: function () {
        return this.getInterestRate() / 12 / 100;
    },

    getN: function () {
        return this.getTerm() * 12;
    },

    getPayment: function () {
        return this.getCost() * ( this.getP() / ( 1 -  Math.pow( 1 + this.getP(), -1 * this.getN() )) );
    },

    focusNextElem: function (e) {
        if (e.keyCode==13) {
            e.preventDefault();
            switch ($(e.target).attr('id')) {
                case 'cost':
                    $('#init-payment').focus();
                    break;
                case 'init-payment':
                    $('#interest-rate').focus();
                    break;
                case 'interest-rate':
                    $('#term').focus();
                    break;
                case 'term':
                    $('#calculation').focus();
            }
        }
    },

    calculation: function () {
        var payment = this.getPayment();

        var pInterest = this.getCost() * this.getP();
        var sBody = payment - pInterest;

        var amountPayments = payment * this.getN();

        this.result.slideDown();
        this.setFirstPayment(payment);
        this.setFirstBody(sBody);
        this.setFirstInterest(payment - sBody);
        this.setAmountPayments(amountPayments);
        this.setOverpayment(amountPayments - this.getCost());

        this.resultChartUpdate(amountPayments);

        this.details.find('#accordion').html('');

        common.scrollTo(this.result);

    },

    // END FORM

    // START RESULT

    result: $('#result'),

    setFirstPayment: function (value) {
        this.result.find('#result-first-payment').html(common.priceEncode(value));
    },

    setFirstBody: function (value) {
        this.result.find('#result-first-body').html(common.priceEncode(value));
    },

    setFirstInterest: function (value) {
        this.result.find('#result-first-interest').html(common.priceEncode(value));
    },

    setOverpayment: function (value) {
        this.result.find('#result-overpayment').html(common.priceEncode(value));
    },

    setAmountPayments: function (value) {
        this.result.find('#result-amount-payments').html(common.priceEncode(value));
    },

    resultChartCtx: document.getElementById("chart-area").getContext("2d"),

    resultChartConfig: {
        type: 'pie',
        data: {
            datasets:[],
            labels: []
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    },

    resultChartUpdate: function (amountPayments) {

        this.resultChartConfig.data.datasets = [];
        this.resultChartConfig.data.labels = [];
        window.myDoughnut.update();

        var newDataset = {
            backgroundColor: [],
            data: [],
            label: 'New dataset ' + this.resultChartConfig.data.datasets.length,
        };

        newDataset.data.push(Math.round(this.getCost()));
        newDataset.backgroundColor.push("#3fad46");
        newDataset.data.push(Math.round(amountPayments - this.getCost()));
        newDataset.backgroundColor.push("#f0ad4e");

        this.resultChartConfig.data.datasets.push(newDataset);
        this.resultChartConfig.data.labels.push("Кредит");
        this.resultChartConfig.data.labels.push("Проценты");
        window.myDoughnut.update();
    },

    // END RESULT

    // START DETAILS

    details: $('#details'),

    detailsShowSelector: '#show-details',

    detailsRender: function () {
        var $accordion = this.details.find('#accordion');
        $accordion.html('');

        var payment = this.getPayment();
        var cost = this.getCost();
        var yearTitle = 1;

        for (var year = this.getTerm(); year > 0; year--) {
            var $tbody = $('<tbody>');
            var yearCost = 0;
            var yearPaymentBody = 0;
            var yearPaymentInterest = 0;
            for (var month = 1; month < 13; month++) {
                var paymentInterest = cost * models.getP();
                var paymentBody = payment - paymentInterest;
                cost -= paymentBody;

                yearCost += payment;
                yearPaymentBody += paymentBody;
                yearPaymentInterest += paymentInterest;

                $tbody.append('<tr>' +
                    '           <td>' + month + '</td>' +
                    '           <td>' + common.priceEncode(paymentBody) + '</td>' +
                    '           <td>' + common.priceEncode(paymentInterest) + '</td>' +
                    '           <td>' + common.priceEncode(payment) + '</td>' +
                    '       </tr>');
            }
            var $divYear = $('<div class="panel panel-default">');
            var $table = $('<table class="table table-hover">');
            $table.append('<thead><tr><td>Мес.</td><td>Тело</td><td>Процент</td><td>Платеж</td></tr></thead>');
            $tbody.append('<tr>' +
                '           <td>Итого</td>' +
                '           <td>' + common.priceEncode(yearPaymentBody) + '</td>' +
                '           <td>' + common.priceEncode(yearPaymentInterest) + '</td>' +
                '           <td>' + common.priceEncode(yearCost) + '</td>' +
                '       </tr>');
            $table.append($tbody);

            var linkHtml = '<div class="row">' +
                '               <div class="col-xs-12">' +
                '                   <strong class="pull-left">' + yearTitle + ' год</strong>' +
                                    '<span class="pull-right">' +
                '                       Выплаты за год &mdash; ' + common.priceEncode(yearPaymentBody) + '<i class="fa fa-rub"></i><br>' +
                                        'Основной долг &mdash; ' + common.priceEncode(yearPaymentInterest) + '<i class="fa fa-rub"></i><br>' +
                                        'Проценты по кредиту &mdash; ' + common.priceEncode(yearCost) + '<i class="fa fa-rub"></i>' +
                '                   </span>' +
                '               </div>' +
                '               <div class="col-xs-12 text-center">' +
                                    '<span class="text-info">Подробней</span>' +
                '               </div>' +
                '           </div>';
            $divYear.append('<div class="panel-heading" role="tab" id="heading-' + year + '">' +
                                '<div class="panel-title">' +
                                    '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-' + year + '" aria-expanded="true" aria-controls="collapse-' + year + '">' +
                                        linkHtml +
                                    '</a>' +
                                '</div>' +
                            '</div>');
            var $divCollapse = $('<div class="panel-collapse collapse">').attr('id','collapse-' + year).attr('role', 'tabpanel').attr('ariaLabelledby', 'heading-' + year);
            var $divPanelBody = $('<div class="panel-body">');
            $divPanelBody.append($table);
            $divCollapse.append($divPanelBody);
            $divYear.append($divCollapse);
            $accordion.append($divYear);
            yearTitle++;

        }

        this.details.slideDown();

        common.scrollTo(this.details);
    },

    // END DETAILS

    // START SLIDER

    sliderInit: function(elementId, inputSelector) {
        var slider = document.getElementById(elementId);

        var $input = this.form.find(inputSelector);

        noUiSlider.create(slider, {
            start: [ $input.val() ],
            step: parseInt($input.attr('step')),
            range: {
                'min': [  parseInt($input.attr('min')) ],
                'max': [ parseInt($input.attr('max')) ]
            }
        });

        slider.noUiSlider.on('update', function( values, handle ) {
            $input.val(values[handle]);
        });

        $input.change(function () {
            slider.noUiSlider.set($(this).val());
        });

        this.sliderDecor(elementId);
    },

    sliderDecor: function (elementId) {
        var $element = $('#' + elementId);
        $element.addClass('progress-striped').addClass('active');
        $element.find('.noUi-base').addClass('progress-bar').addClass('progress-bar-warning');
        $element.find('.noUi-origin').css('background-color', '#fff');
    }

    // END SLIDER
};