<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 27.08.16
 * Time: 22:14
 */
?>


<div id="main" class="row">
    <div class="col-xs-12">
        <h1><?= META_H1_MAIN; ?></h1>

        <?php require 'adsense' . DIRECTORY_SEPARATOR . 'first.php'; ?>

        <p  class="form-group">
            <strong>mycalculate.ru</strong> - это лёгкий и быстрый способ расчета суммы переплаты, платежа, реальной и эффективной
            процентной ставки по кредиту. Так же калькулятор поможет вам разобраться, какая часть выплат идет на
            погашение основной кредитной суммы, а какая часть на погашение процентов по кредиту и ипотеки.</p>
        <div class="row text-center">

            <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                    <img src="/image/mortgage.png" alt="<?= APP_CREDIT_NAME_MORTGAGE; ?>">
                    <div class="caption">
                        <h3><?= APP_CREDIT_NAME_MORTGAGE; ?></h3>
                        <p>
                            <a href="/mortgage" class="btn btn-success" role="button" alt="<?= APP_CREDIT_LINK_NAME_MORTGAGE; ?>">
                                <?= BUTTON_CALCULATE; ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                    <img src="/image/consumer.png" alt="<?= APP_CREDIT_NAME_CONSUMER; ?>">
                    <div class="caption">
                        <h3><?= APP_CREDIT_NAME_CONSUMER; ?></h3>
                        <p>
                            <a href="/consumer" class="btn btn-success" role="button" alt="<?= APP_CREDIT_LINK_NAME_CONSUMER; ?>">
                                <?= BUTTON_CALCULATE; ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <?php require 'adsense' . DIRECTORY_SEPARATOR . 'second.php'; ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                    <img src="/image/auto.png" alt="<?= APP_CREDIT_NAME_AUTO; ?>">
                    <div class="caption">
                        <h3><?= APP_CREDIT_NAME_AUTO; ?></h3>
                        <p>
                            <a href="/auto" class="btn btn-success" role="button" alt="<?= APP_CREDIT_LINK_NAME_AUTO; ?>">
                                <?= BUTTON_CALCULATE; ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="thumbnail">
                    <img src="/image/business.png" alt="<?= APP_CREDIT_NAME_BUSINESS; ?>" >
                    <div class="caption">
                        <h3><?= APP_CREDIT_NAME_BUSINESS; ?></h3>
                        <p>
                            <a href="/business" class="btn btn-success" role="button" alt="<?= APP_CREDIT_LINK_NAME_BUSINESS; ?>">
                                <?= BUTTON_CALCULATE; ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <?php require 'adsense' . DIRECTORY_SEPARATOR . 'third.php'; ?>
            </div>

        </div>
    </div>
</div>
