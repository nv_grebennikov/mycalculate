<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 21.08.16
 * Time: 13:14
 */
?>


<div class="row">
    <div class="col-xs-12">
        <h1><?= META_H1_CONSUMER; ?></h1>

        <?php require 'adsense' . DIRECTORY_SEPARATOR . 'first.php'; ?>

        <div class="row">
            <div class="col-xs-12 col-md-7">
                <form id="form-credit">

                    <div id="form-error" class="alert alert-danger hidden" role="alert"></div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="cost">
                                <?= APP_FORM_COST; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-cost"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="cost" name="cost" required step="5000" min="50000" max="1000000" value="300000">
                                <div class="input-group-addon"><i class="fa fa-rub"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="cost">
                                <?= APP_FORM_INIT_PAYMENT; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-init-payment"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="init-payment" name="init-payment" required step="5000" min="0" max="700000" value="50000">
                                <div class="input-group-addon"><i class="fa fa-rub"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="cost">
                                <?= APP_FORM_INTEREST_RATE; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-interest-rate"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="interest-rate" name="interest-rate" required step="0.01" min="12" max="30" value="15">
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="term">
                                <?= APP_FORM_TERM; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-term"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="term" name="term" required step="1" min="1" max="7" value="5">
                                <div class="input-group-addon">Лет</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <a id="calculation" href="/" class="btn btn-success"><?= APP_FORM_BUTTON; ?></a>
                    </div>

                </form>
            </div>
            <div class="col-xs-12 col-md-5">
                <?php require 'adsense' . DIRECTORY_SEPARATOR . 'second.php'; ?>
            </div>
        </div>

    </div>
</div>

<?php require '_result.php'; ?>

<?php require '_details.php'; ?>

<div class="row">
    <section class="col-xs-12">
        <header>
            <h2>Что нужно знать о потребительском кредите</h2>
        </header>
        <?php require 'adsense' . DIRECTORY_SEPARATOR . 'third.php'; ?>
        <section>
            <div class="well">
                <p>
                    Наверное, каждый в своей жизни сталкивался с ситуацией срочной необходимости денег. И если еще десяток лет назад единственной возможностью был займ у знакомых, то сегодня все большую популярность набирает потребительское кредитование. Именно оно позволяет в кратчайшие сроки и с минимальными усилиями получить нужную сумму.
                </p>
            </div>

            <section>
                <header>
                    <h3>В чем особенности потребительского кредита?</h3>
                </header>
                <section>
                    <div class="well">
                        <p>Одной из главных особенностей, отличающих потребительский займ от иных программ кредитования, является тесная взаимосвязь его условий с конкретным заемщиком. Так как на вопрос о выдаче кредита влияет огромное число факторов: от положительной кредитной истории и доходов до личности самого заемщика, часто случается, что два клиента получают в одном банке ссуды на абсолютно разных условиях. Причем, различаться может не только размер суммы, но и процентная ставка со сроками погашения. </p>
                        <div class="collapse" id="consumer-1">
                            <p>Интересно, что потребительский кредит может быть как в виде денежных средств, так и сразу в виде товаров. Подобная практика развита в розничных торговых точках.</p>
                            <p>Еще одной отличительной чертой данного вида кредитования выступает его универсальность. Сегодня получить потребительский займ может практически любой человек, независимо от принадлежности к социальной группе. Во многих банках имеются специальные программы, направленные на поддержку определенных категорий лиц, например, студентов или пенсионеров. При этом клиент имеет возможность выбрать, как целевое кредитование на приобретение конкретного товара или услуги, либо нецелевую ссуду, которую он может использовать по своему усмотрению.</p>
                            <p>Характерная черта потребительского кредита – возможность его частого оформления только на основании паспорта и ИНН. У вас не потребуют не только залог, но и справку о доходах. Несмотря на привлекательность такого займа простотой процедуры его получения, не стоит забывать, что «расплатиться» за доверие финансового учреждения придется повышенной процентной ставкой.</p>
                            <p>Что касается порядка погашения кредитной задолженности, то он, как и при обычном займе, может осуществляться путем дифференцированных либо аннуитетных платежей. Также возможен досрочный возврат долга. В большинстве случаев заемщик может самостоятельно выбрать приемлемый для себя способ погашения кредита.</p>
                            <p>Обилие программ, предлагаемых финансовыми учреждениями и различными онлайн-сервисами, дает возможность каждому подобрать наиболее оптимальные условия потребительского кредитования, что обуславливает его растущую популярность среди населения.</p>
                        </div>
                        <button class="btn btn-info btn-sm" type="button" data-toggle="collapse" data-target="#consumer-1" aria-expanded="false" aria-controls="consumer-1">
                            Читать далее
                        </button>
                    </div>
                </section>
            </section>

        </section>
    </section>
</div>