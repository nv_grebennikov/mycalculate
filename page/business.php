<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 22.08.16
 * Time: 21:00
 */
?>

<div class="row">
    <div class="col-xs-12">
        <h1><?= META_H1_BUSINESS; ?></h1>

        <?php require 'adsense' . DIRECTORY_SEPARATOR . 'first.php'; ?>

        <div class="row">
            <div class="col-xs-12 col-md-7">
                <form id="form-credit">

                    <div id="form-error" class="alert alert-danger hidden" role="alert"></div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="cost">
                                <?= APP_FORM_COST; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-cost"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="cost" name="cost" required step="10000" min="100000" max="5000000" value="700000">
                                <div class="input-group-addon"><i class="fa fa-rub"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="cost">
                                <?= APP_FORM_INIT_PAYMENT; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-init-payment"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="init-payment" name="init-payment" required step="10000" min="50000" max="4500000" value="250000">
                                <div class="input-group-addon"><i class="fa fa-rub"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="cost">
                                <?= APP_FORM_INTEREST_RATE; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-interest-rate"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="interest-rate" name="interest-rate" required step="0.01" min="6" max="25" value="15">
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label for="term">
                                <?= APP_FORM_TERM; ?>
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8 slider-p">
                            <div id="slider-term"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="input-group">
                                <input type="number" class="form-control" id="term" name="term" required step="1" min="1" max="5" value="3">
                                <div class="input-group-addon">Лет</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <a id="calculation" href="/" class="btn btn-success"><?= APP_FORM_BUTTON; ?></a>
                    </div>

                </form>
            </div>
            <div class="col-xs-12 col-md-5">
                <?php require 'adsense' . DIRECTORY_SEPARATOR . 'second.php'; ?>
            </div>
        </div>

    </div>
</div>

<?php require '_result.php'; ?>

<?php require '_details.php'; ?>

<div class="row">
    <section class="col-xs-12">
        <header>
            <h2>Кредитование предприятий среднего и малого бизнеса: основные особенности</h2>
        </header>
        <?php require 'adsense' . DIRECTORY_SEPARATOR . 'third.php'; ?>
        <section>
            <div class="well">
                <p>Сегодня средний и малый бизнес выступает важным плацдармом в развитии национальной экономики. Учитывая традиционно высокую потребность данных секторов в заёмных средствах для своего развития, неудивительно, что всё чаще банки разрабатывают для них специальные программы кредитования. В чём особенности таких займов?</p>
            </div>
            <section>
                <header>
                    <h3>Факторы, усложняющие получение кредитов</h3>
                </header>
                <section>
                    <div class="well">
                        <p>В данный момент около 80% финансовых учреждений предлагает возможность выдачи кредитов в пользу среднего и малого бизнеса. Несмотря на это, существует несколько причин, почему предприятиям может быть сложно получить займ:</p>
                        <div class="collapse" id="business-1">
                            <em>Слабая легитимность</em>
                            <p>Не секрет, что многим фирмам малого или среднего секторов экономики в целях уменьшения налоговой нагрузки приходится идти на некоторые уловки, снижающие «прозрачность» их работы. Это может привести к тому, что банк посчитает деятельность предприятия недостаточно легитимной и откажет в кредитовании.</p>
                            <p>Поэтому стоит тщательно взвесить, не перевесят ли выросшую нагрузку по налогам возможности, открывающиеся после получения займа.</p>
                            <em>Невысокая рентабельность предприятия</em>
                            <p>В этом случае нужно особо тщательно подойти к выбору банка. Сегодня существует ряд финансовых учреждений, к примеру, МСП банк, предлагающих различные программы поддержки данных секторов. Это поможет получить гораздо более низкую процентную ставку.</p>
                            <em>Необходимость предоставления залога</em>
                            <p>Традиционно финансовыми структурами в качестве залога рассматривается имущество значительной стоимости: недвижимость, оборудование, транспорт. А предприятия малого и среднего бизнеса зачастую характеризуются недостаточной стоимостью имеющихся активов.</p>
                            <p>В качестве альтернативы можно посоветовать рассмотреть возможность получения беззалогового займа. Но в этом случае нужно быть готовым заплатить повышенную ставку по кредиту. Также, существует ограничение и по максимальной сумме. Как правило, она не превышает 1 000 000 рублей.</p>
                        </div>
                        <button class="btn btn-info btn-sm" type="button" data-toggle="collapse" data-target="#business-1" aria-expanded="false" aria-controls="business-1">
                            Читать далее
                        </button>
                    </div>
                </section>
            </section>
            <section>
                <header>
                    <h3>Как выбрать банк?</h3>
                </header>
                <section>
                    <div class="well">
                        <p>При выборе финансового учреждения, прежде всего, стоит обратить внимание на следующие особенности.</p>
                        <div class="collapse" id="business-2">
                            <em>Основную направленность деятельности банка.</em>
                            <p>Крупные финансовые структуры зачастую нацелены на сотрудничество с крупным бизнесом. Они не откажут в рассмотрении заявки на займ от предприятия среднего или малого секторов. Однако предлагаемые условия будут типовыми, без возможности их изменения исходя из потребностей и возможностей конкретной фирмы.</p>
                            <p>А вот небольшие региональные банки гораздо охотнее идут на уступки своим клиентам и проявляют значительную гибкость в предъявлении требований.</p>
                            <em>Наличие специализированных программ</em>
                            <p>Это может быть, к примеру, кредит на авто или открытие овердрафта. Учитывая их целевое назначение, предприятию проще понять, удовлетворяет ли оно прописанным условиям.</p>
                            <p>Обратите внимание, что программы каждого банка отличаются не только процентной ставкой, суммой и сроком кредита, но и перечнем необходимых документов и размером оплачиваемых комиссий.</p>
                            <p>Так, помимо комиссии за открытие и ведение ссудного счета, во многих банках придётся заплатить и за рассмотрение кредитной заявки. Оформление беззалогового займа также может вылиться в дополнительные расходы. Учтите, что обычно требуется перевод финансового оборота предприятия или его части в учреждение, выдающее займ. Следовательно, стоит заранее узнать стоимость расчетно-кассового обслуживания.</p>
                            <p>Также не забудьте уточнить размер штрафов и пени, которые могут возникнуть в случае просрочки ежемесячных кредитных платежей.</p>
                            <p>Как видим, если предприятие перед обращением в банк не поленится проделать определенную подготовительную работу, у него есть все шансы выбрать наиболее лояльные условия и извлечь максимальную выгоду из полученного кредита.</p>
                        </div>
                        <button class="btn btn-info btn-sm" type="button" data-toggle="collapse" data-target="#business-2" aria-expanded="false" aria-controls="business-2">
                            Читать далее
                        </button>
                    </div>
                </section>
            </section>
        </section>
    </section>
</div>


