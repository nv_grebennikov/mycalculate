<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 27.08.16
 * Time: 22:18
 */
?>

<div id="result" class="row" hidden>
    <div class="col-xs-12 col-md-6">
        <h2>Результаты расчета</h2>
        <p>
            Первый платеж &mdash; <span id="result-first-payment"></span> <i class="fa fa-rub"></i><br>
            <small>Тело кредита &mdash; <span id="result-first-body"></span> <i class="fa fa-rub"></i></small><br>
            <small>Проценты по кредиту &mdash; <span id="result-first-interest"></span> <i class="fa fa-rub"></i></small>
        </p>
        <p>
            Переплата по кредиту &mdash; <span id="result-overpayment"></span> <i class="fa fa-rub"></i><br>
            Сумма выплаты &mdash; <span id="result-amount-payments"></span> <i class="fa fa-rub"></i>
        </p>
    </div>
    <div class="col-xs-12 col-md-6">
        <canvas id="chart-area" style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select: none;" />
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a id="show-details" href="#" class="btn btn-sm btn-info" data-id="#details">Детали</a>
            <a id="reload" href="#" class="btn btn-sm btn-warning">Сбросить</a>
        </div>
    </div>
</div>
