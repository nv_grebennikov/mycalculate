<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 21.08.16
 * Time: 12:27
 */
?>

<div class="col-xs-12 text-center">
    <h1 style="font-size: 150px"><?= META_H1_ERROR; ?></h1>
    <p class="lead">Страница не найдена</p>
    <a href="/" class="btn btn-default btn-lg btn-block">Перейти на главную</a>
</div>
